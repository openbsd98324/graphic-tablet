# Graphic-Tablet

![](medias/graphic-tablet.png)


````
Ubuntu

    Bamboo, Bamboo1, & BambooFun (mit/ohne Touch)

    Cintiq (4 und 5)

    CintiqPartner

    Graphire

    Intuos 1, 2, 3 und 4

    PenPartner

    PL & DTF

    TabletPC (mit/ohne Touch)

    Volito series

    Intuos 5 mit Touch (PTH-450, PTH-650, PTH-850)

    Intuos 5 ohne Touch (PTK-650)

    Bamboo Pen & Touch, Bamboo Pen, Bamboo Fun Pen & Touch (CTL-470K, CTH-470K, CTH-470S, CTH-670S)

    Wireless-Kit für Bamboo (3. Gen.) und Intuos 5

    Intuos Pen (CTL-480, CTL-680)

    Intuos Pen & Touch (CTH-480, CTH-680)

    Intuos Pro (PTH-451, PTH-651, PTH-851)



````


